﻿namespace OOP_Analiza_LV6_1_V2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonSqareRoot = new System.Windows.Forms.Button();
            this.buttonSquare = new System.Windows.Forms.Button();
            this.buttonCosine = new System.Windows.Forms.Button();
            this.buttonSine = new System.Windows.Forms.Button();
            this.buttonLogarithm = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonNine = new System.Windows.Forms.Button();
            this.buttonEight = new System.Windows.Forms.Button();
            this.buttonSeven = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonSubtract = new System.Windows.Forms.Button();
            this.buttonSix = new System.Windows.Forms.Button();
            this.buttonFive = new System.Windows.Forms.Button();
            this.buttonFour = new System.Windows.Forms.Button();
            this.buttonMultiply = new System.Windows.Forms.Button();
            this.buttonThree = new System.Windows.Forms.Button();
            this.buttonTwo = new System.Windows.Forms.Button();
            this.buttonOne = new System.Windows.Forms.Button();
            this.buttonDivide = new System.Windows.Forms.Button();
            this.buttonEqual = new System.Windows.Forms.Button();
            this.buttonZero = new System.Windows.Forms.Button();
            this.buttonDot = new System.Windows.Forms.Button();
            this.textBoxResult = new System.Windows.Forms.TextBox();
            this.label_operation = new System.Windows.Forms.Label();
            this.label_operator_clicked = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonSqareRoot
            // 
            this.buttonSqareRoot.Location = new System.Drawing.Point(14, 71);
            this.buttonSqareRoot.Name = "buttonSqareRoot";
            this.buttonSqareRoot.Size = new System.Drawing.Size(54, 53);
            this.buttonSqareRoot.TabIndex = 0;
            this.buttonSqareRoot.Text = "sqrt";
            this.buttonSqareRoot.UseVisualStyleBackColor = true;
            this.buttonSqareRoot.Click += new System.EventHandler(this.operator_click);
            // 
            // buttonSquare
            // 
            this.buttonSquare.Location = new System.Drawing.Point(74, 71);
            this.buttonSquare.Name = "buttonSquare";
            this.buttonSquare.Size = new System.Drawing.Size(54, 53);
            this.buttonSquare.TabIndex = 1;
            this.buttonSquare.Text = "x^2";
            this.buttonSquare.UseVisualStyleBackColor = true;
            this.buttonSquare.Click += new System.EventHandler(this.operator_click);
            // 
            // buttonCosine
            // 
            this.buttonCosine.Location = new System.Drawing.Point(194, 71);
            this.buttonCosine.Name = "buttonCosine";
            this.buttonCosine.Size = new System.Drawing.Size(54, 53);
            this.buttonCosine.TabIndex = 3;
            this.buttonCosine.Text = "cos";
            this.buttonCosine.UseVisualStyleBackColor = true;
            this.buttonCosine.Click += new System.EventHandler(this.operator_click);
            // 
            // buttonSine
            // 
            this.buttonSine.Location = new System.Drawing.Point(134, 71);
            this.buttonSine.Name = "buttonSine";
            this.buttonSine.Size = new System.Drawing.Size(54, 53);
            this.buttonSine.TabIndex = 2;
            this.buttonSine.Text = "sin";
            this.buttonSine.UseVisualStyleBackColor = true;
            this.buttonSine.Click += new System.EventHandler(this.operator_click);
            // 
            // buttonLogarithm
            // 
            this.buttonLogarithm.Location = new System.Drawing.Point(254, 71);
            this.buttonLogarithm.Name = "buttonLogarithm";
            this.buttonLogarithm.Size = new System.Drawing.Size(54, 53);
            this.buttonLogarithm.TabIndex = 4;
            this.buttonLogarithm.Text = "log";
            this.buttonLogarithm.UseVisualStyleBackColor = true;
            this.buttonLogarithm.Click += new System.EventHandler(this.operator_click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Location = new System.Drawing.Point(254, 130);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(54, 53);
            this.buttonDelete.TabIndex = 9;
            this.buttonDelete.Text = "DEL";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.button_clear);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(194, 130);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(54, 53);
            this.buttonAdd.TabIndex = 8;
            this.buttonAdd.Text = "+";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.operator_click);
            // 
            // buttonNine
            // 
            this.buttonNine.Location = new System.Drawing.Point(134, 130);
            this.buttonNine.Name = "buttonNine";
            this.buttonNine.Size = new System.Drawing.Size(54, 53);
            this.buttonNine.TabIndex = 7;
            this.buttonNine.Text = "9";
            this.buttonNine.UseVisualStyleBackColor = true;
            this.buttonNine.Click += new System.EventHandler(this.button_click);
            // 
            // buttonEight
            // 
            this.buttonEight.Location = new System.Drawing.Point(74, 130);
            this.buttonEight.Name = "buttonEight";
            this.buttonEight.Size = new System.Drawing.Size(54, 53);
            this.buttonEight.TabIndex = 6;
            this.buttonEight.Text = "8";
            this.buttonEight.UseVisualStyleBackColor = true;
            this.buttonEight.Click += new System.EventHandler(this.button_click);
            // 
            // buttonSeven
            // 
            this.buttonSeven.Location = new System.Drawing.Point(14, 130);
            this.buttonSeven.Name = "buttonSeven";
            this.buttonSeven.Size = new System.Drawing.Size(54, 53);
            this.buttonSeven.TabIndex = 5;
            this.buttonSeven.Text = "7";
            this.buttonSeven.UseVisualStyleBackColor = true;
            this.buttonSeven.Click += new System.EventHandler(this.button_click);
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(254, 189);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(54, 171);
            this.buttonClear.TabIndex = 14;
            this.buttonClear.Text = "CE";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.button_clear);
            // 
            // buttonSubtract
            // 
            this.buttonSubtract.Location = new System.Drawing.Point(194, 189);
            this.buttonSubtract.Name = "buttonSubtract";
            this.buttonSubtract.Size = new System.Drawing.Size(54, 53);
            this.buttonSubtract.TabIndex = 13;
            this.buttonSubtract.Text = "-";
            this.buttonSubtract.UseVisualStyleBackColor = true;
            this.buttonSubtract.Click += new System.EventHandler(this.operator_click);
            // 
            // buttonSix
            // 
            this.buttonSix.Location = new System.Drawing.Point(134, 189);
            this.buttonSix.Name = "buttonSix";
            this.buttonSix.Size = new System.Drawing.Size(54, 53);
            this.buttonSix.TabIndex = 12;
            this.buttonSix.Text = "6";
            this.buttonSix.UseVisualStyleBackColor = true;
            this.buttonSix.Click += new System.EventHandler(this.button_click);
            // 
            // buttonFive
            // 
            this.buttonFive.Location = new System.Drawing.Point(74, 189);
            this.buttonFive.Name = "buttonFive";
            this.buttonFive.Size = new System.Drawing.Size(54, 53);
            this.buttonFive.TabIndex = 11;
            this.buttonFive.Text = "5";
            this.buttonFive.UseVisualStyleBackColor = true;
            this.buttonFive.Click += new System.EventHandler(this.button_click);
            // 
            // buttonFour
            // 
            this.buttonFour.Location = new System.Drawing.Point(14, 189);
            this.buttonFour.Name = "buttonFour";
            this.buttonFour.Size = new System.Drawing.Size(54, 53);
            this.buttonFour.TabIndex = 10;
            this.buttonFour.Text = "4";
            this.buttonFour.UseVisualStyleBackColor = true;
            this.buttonFour.Click += new System.EventHandler(this.button_click);
            // 
            // buttonMultiply
            // 
            this.buttonMultiply.Location = new System.Drawing.Point(194, 248);
            this.buttonMultiply.Name = "buttonMultiply";
            this.buttonMultiply.Size = new System.Drawing.Size(54, 53);
            this.buttonMultiply.TabIndex = 18;
            this.buttonMultiply.Text = "*";
            this.buttonMultiply.UseVisualStyleBackColor = true;
            this.buttonMultiply.Click += new System.EventHandler(this.operator_click);
            // 
            // buttonThree
            // 
            this.buttonThree.Location = new System.Drawing.Point(134, 248);
            this.buttonThree.Name = "buttonThree";
            this.buttonThree.Size = new System.Drawing.Size(54, 53);
            this.buttonThree.TabIndex = 17;
            this.buttonThree.Text = "3";
            this.buttonThree.UseVisualStyleBackColor = true;
            this.buttonThree.Click += new System.EventHandler(this.button_click);
            // 
            // buttonTwo
            // 
            this.buttonTwo.Location = new System.Drawing.Point(74, 248);
            this.buttonTwo.Name = "buttonTwo";
            this.buttonTwo.Size = new System.Drawing.Size(54, 53);
            this.buttonTwo.TabIndex = 16;
            this.buttonTwo.Text = "2";
            this.buttonTwo.UseVisualStyleBackColor = true;
            this.buttonTwo.Click += new System.EventHandler(this.button_click);
            // 
            // buttonOne
            // 
            this.buttonOne.Location = new System.Drawing.Point(14, 248);
            this.buttonOne.Name = "buttonOne";
            this.buttonOne.Size = new System.Drawing.Size(54, 53);
            this.buttonOne.TabIndex = 15;
            this.buttonOne.Text = "1";
            this.buttonOne.UseVisualStyleBackColor = true;
            this.buttonOne.Click += new System.EventHandler(this.button_click);
            // 
            // buttonDivide
            // 
            this.buttonDivide.Location = new System.Drawing.Point(194, 307);
            this.buttonDivide.Name = "buttonDivide";
            this.buttonDivide.Size = new System.Drawing.Size(54, 53);
            this.buttonDivide.TabIndex = 23;
            this.buttonDivide.Text = "/";
            this.buttonDivide.UseVisualStyleBackColor = true;
            this.buttonDivide.Click += new System.EventHandler(this.operator_click);
            // 
            // buttonEqual
            // 
            this.buttonEqual.Location = new System.Drawing.Point(134, 307);
            this.buttonEqual.Name = "buttonEqual";
            this.buttonEqual.Size = new System.Drawing.Size(54, 53);
            this.buttonEqual.TabIndex = 22;
            this.buttonEqual.Text = "=";
            this.buttonEqual.UseVisualStyleBackColor = true;
            this.buttonEqual.Click += new System.EventHandler(this.button_equal);
            // 
            // buttonZero
            // 
            this.buttonZero.Location = new System.Drawing.Point(74, 307);
            this.buttonZero.Name = "buttonZero";
            this.buttonZero.Size = new System.Drawing.Size(54, 53);
            this.buttonZero.TabIndex = 21;
            this.buttonZero.Text = "0";
            this.buttonZero.UseVisualStyleBackColor = true;
            this.buttonZero.Click += new System.EventHandler(this.button_click);
            // 
            // buttonDot
            // 
            this.buttonDot.Location = new System.Drawing.Point(14, 307);
            this.buttonDot.Name = "buttonDot";
            this.buttonDot.Size = new System.Drawing.Size(54, 53);
            this.buttonDot.TabIndex = 20;
            this.buttonDot.Text = ",";
            this.buttonDot.UseVisualStyleBackColor = true;
            this.buttonDot.Click += new System.EventHandler(this.button_click);
            // 
            // textBoxResult
            // 
            this.textBoxResult.Location = new System.Drawing.Point(14, 45);
            this.textBoxResult.Name = "textBoxResult";
            this.textBoxResult.Size = new System.Drawing.Size(294, 20);
            this.textBoxResult.TabIndex = 24;
            this.textBoxResult.Text = "0";
            // 
            // label_operation
            // 
            this.label_operation.AutoSize = true;
            this.label_operation.Location = new System.Drawing.Point(12, 29);
            this.label_operation.Name = "label_operation";
            this.label_operation.Size = new System.Drawing.Size(0, 13);
            this.label_operation.TabIndex = 25;
            this.label_operation.Click += new System.EventHandler(this.labelOperation);
            // 
            // label_operator_clicked
            // 
            this.label_operator_clicked.AutoSize = true;
            this.label_operator_clicked.Location = new System.Drawing.Point(11, 29);
            this.label_operator_clicked.Name = "label_operator_clicked";
            this.label_operator_clicked.Size = new System.Drawing.Size(0, 13);
            this.label_operator_clicked.TabIndex = 26;
            this.label_operator_clicked.Click += new System.EventHandler(this.operator_click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(318, 366);
            this.Controls.Add(this.label_operator_clicked);
            this.Controls.Add(this.label_operation);
            this.Controls.Add(this.textBoxResult);
            this.Controls.Add(this.buttonDivide);
            this.Controls.Add(this.buttonEqual);
            this.Controls.Add(this.buttonZero);
            this.Controls.Add(this.buttonDot);
            this.Controls.Add(this.buttonMultiply);
            this.Controls.Add(this.buttonThree);
            this.Controls.Add(this.buttonTwo);
            this.Controls.Add(this.buttonOne);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.buttonSubtract);
            this.Controls.Add(this.buttonSix);
            this.Controls.Add(this.buttonFive);
            this.Controls.Add(this.buttonFour);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.buttonNine);
            this.Controls.Add(this.buttonEight);
            this.Controls.Add(this.buttonSeven);
            this.Controls.Add(this.buttonLogarithm);
            this.Controls.Add(this.buttonCosine);
            this.Controls.Add(this.buttonSine);
            this.Controls.Add(this.buttonSquare);
            this.Controls.Add(this.buttonSqareRoot);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonSqareRoot;
        private System.Windows.Forms.Button buttonSquare;
        private System.Windows.Forms.Button buttonCosine;
        private System.Windows.Forms.Button buttonSine;
        private System.Windows.Forms.Button buttonLogarithm;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonNine;
        private System.Windows.Forms.Button buttonEight;
        private System.Windows.Forms.Button buttonSeven;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonSubtract;
        private System.Windows.Forms.Button buttonSix;
        private System.Windows.Forms.Button buttonFive;
        private System.Windows.Forms.Button buttonFour;
        private System.Windows.Forms.Button buttonMultiply;
        private System.Windows.Forms.Button buttonThree;
        private System.Windows.Forms.Button buttonTwo;
        private System.Windows.Forms.Button buttonOne;
        private System.Windows.Forms.Button buttonDivide;
        private System.Windows.Forms.Button buttonEqual;
        private System.Windows.Forms.Button buttonZero;
        private System.Windows.Forms.Button buttonDot;
        private System.Windows.Forms.TextBox textBoxResult;
        private System.Windows.Forms.Label label_operation;
        private System.Windows.Forms.Label label_operator_clicked;
    }
}

