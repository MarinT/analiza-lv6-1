﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP_Analiza_LV6_1_V2
{
    public partial class Form1 : Form
    {

        Double result = 0;
        String operatorChar = "";
        bool isOperationPerformed = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void button_click(object sender, EventArgs e) // event kojeg dijele gumbovi vezani uz unos brojeva u textBox
        {
            if ((textBoxResult.Text == "0") || (isOperationPerformed)) // if uvjet koji vodi reda o "higijeni" textBoxa
                textBoxResult.Clear();

            isOperationPerformed = false; // bool operator koji javlja je li se aritmeticka operacija izvrsila

            Button button = (Button)sender; 
            if(button.Text ==",") // if uvjet koji pazi kako ne bismo ponavljali decimalni zarez
            {
                if(textBoxResult.Text.Contains(","))
                    textBoxResult.Text = textBoxResult.Text + button.Text;
            }
            else
            textBoxResult.Text = textBoxResult.Text + button.Text;

           

        }

        private void operator_click(object sender, EventArgs e) // event kojeg dijele aritmeticki operatori
        {
            Button button = (Button)sender;
            operatorChar = button.Text;
            result = Double.Parse(textBoxResult.Text);
            label_operator_clicked.Text = result + " " + operatorChar; // ispis broja i aritmetickog operatora
            isOperationPerformed = true;

        }

        private void button_clear(object sender, EventArgs e) // event kojeg dijele clear i delete gumbovi
        {

            Button button = (Button)sender;

            if (button.Text == "CE") // potpuno brise textBox i resetira vrijednosti 
            {
                textBoxResult.Text = "0";
                result = 0;
            }

            else { // vezano uz delete gumb, brise pojedine znamenke 
                if (textBoxResult.Text.Length>1)
                textBoxResult.Text = textBoxResult.Text.Remove(textBoxResult.Text.Length - 1);

                else
                    textBoxResult.Text = "0";
            }

        }

        private void button_equal(object sender, EventArgs e) // event iskljucivo za gumb jednako
        {
            switch(operatorChar) // switch case koji primjenjuje adekvatnu operaciju ravnajuci se po textu kojeg je primio od kliknutog gumba
            {
                case "+":
                    textBoxResult.Text = (result + Double.Parse(textBoxResult.Text)).ToString();
                    break;
                case "-":
                    textBoxResult.Text = (result - Double.Parse(textBoxResult.Text)).ToString();
                    break;
                case "*":
                    textBoxResult.Text = (result * Double.Parse(textBoxResult.Text)).ToString();
                    break;
                case "/":
                    textBoxResult.Text = (result / Double.Parse(textBoxResult.Text)).ToString();
                    break;
                case "sin":
                    textBoxResult.Text = Math.Sin(Double.Parse(textBoxResult.Text)).ToString();
                    break;
                case "cos":
                    textBoxResult.Text = Math.Cos(Double.Parse(textBoxResult.Text)).ToString();
                    break;
                case "sqrt":
                    textBoxResult.Text = Math.Sqrt(Double.Parse(textBoxResult.Text)).ToString();
                    break;
                case "x^2":
                    textBoxResult.Text = Math.Pow(Double.Parse(textBoxResult.Text),2).ToString();
                    break;
                case "log":
                    textBoxResult.Text = Math.Log(Double.Parse(textBoxResult.Text),10).ToString();
                    break;




            }
        }

        private void labelOperation(object sender, EventArgs e)
        {

        }
    }
}
